# ZLamantin

ZLamantin est une petite librairie qui permet de créer des consoles en PHP CLI.

Tous les composants de cette librairie se trouvent dans le namespace `com\darksidegames\zlamantin`.

## Index
<span id="__index"></span>

- [Fonctionnement](#__fonctionnement)
- [Exemple](#__exemple)

- [La classe `zlShell`](#_la-classe-zlshell)
	- [Le prompt](#__prompt)
	- [Les caractères spéciaux](#__spcchars)
	- [Les options](#__options)
	- [L'intro](#__intro)
	- [La pile de commandes](#__cmdspile)

## Fonctionnement
<span id="__fonctionnement"></span>

Pour commencer, il faut créer un terminal. Pour cela, il existe deux méthodes :
1. Créer une classe qui hérite de `zlShell`, puis créer une instance de cette classe qui sera démarrée. Cette méthode a pour avantage de permettre de redéfinir les méthodes natives de `zlShell`. Cependant, si vous n'avez pas besoin de les redéfinir, mieux vaut vous tourner vers la deuxième méthode, plus simple.
2. Créer directement une instance de la classe `zlShell` qui sera démarrée.

Une fois le terminal créé, peu importe la méthode, il faut y enregistrer des commandes. Cela se fait _via_ la méthode `zlShell->registerCommand`. Les commandes sont des classes qui héritent de la classe abstraite `zlCommand`.

De la même façon on peut ajouter des topics d'aide. Les topics d'aide, eux, héritent de la classe `zlHelpTopic`, et sont enregistrés _via_ la méthode `zlShell->registerHelpTopic`. Notez que l'aide ne sera disponible que si vous enregistrez la commande prédéfinie `zlHelpCommand` dans le terminal ou que vous créez votre propre commande d'aide.

	$o->registerCommand("help", new zlHelpCommand);

Une fois les commandes et les topics d'aide enregistrés, il faut lancer la boucle du terminal. Pour cela, utilisez la méthode `zlShell->start`. Cependant, il est possible d'enregistrer des commandes et des topics d'aide après le démarrage de la boucle (dans les méthodes des commandes, par exemple).

## Exemple
<span id="__exemple"></span>
```php
<?php
require_once "ZLamantin.php";

use com\darksidegames\zlamantin\zlShell;
use com\darksidegames\zlamantin\zlCommand;
use com\darksidegames\zlamantin\zlDocumentedCommand;

class MyExitCommand extends zlCommand {
	protected $validityMasks = [
		"/^\/\?$/", // mask index 0
		"/^$/"      // mask index 1
	];

	public function action(string $cmd, string $arg, bool &$stop, zlShell &$shell, int $maskIndex, array $maskArgs) {
		switch ($maskIndex) {
		case 0:
			$this->help($cmd, $shell);
			break;
		case 1:
			$bool = true;
			break;
		}
		return;
	}

	use zlDocumentedCommand;
	public function help(string $cmd, zlShell &$shell) {
		fprintf(STDOUT, "Commande $cmd" . PHP_EOL);
		fprintf(STDOUT, "Ferme le terminal." . PHP_EOL);
		fprintf(STDOUT, "Syntaxe : " . PHP_EOL);
		fprintf(STDOUT, "  $cmd" . PHP_EOL);
		fprintf(STDOUT, "  $cmd /?" . PHP_EOL);
	}
}

class EchoCommand extends zlCommand {
	protected $validityMasks = [
		"/^(.+)$/"
	];

	public function action(string $cmd, string $arg, bool &$stop, zlShell &$shell, int $maskIndex, array $maskArgs) {
		switch ($maskIndex) {
		case 0:
			fprintf(STDOUT, $maskArgs[0]);
			break;
		}
		return;
	}
}

$myTerminal = new zlShell();

$myTerminal->registerCommand("exit", new MyExitCommand());
$myTerminal->registerCommand("echo", new EchoCommand());

$myTerminal->start();
```

---

# La classe `zlShell`
<span id="_la-classe-zlshell"></span>

La classe `zlShell` représente un terminal.

## Le prompt
<span id="__prompt"></span>

Pour changer le prompt, utilisez la méthode `zlShell->setPrompt` :

	$terminal->setPrompt("> ");

Vous pouvez utiliser les caractères spéciaux, qui seront parsés si l'option `treatEscapedChars` est activé.

Pour obtenir le prompt, utilisez la méthode `zlShell->getPrompt` :

	$terminal->getPrompt();

## Les caractères spéciaux
<span id="__spcchars"></span>

Il existe des caractères spéciaux, qui peuvent être parsés si l'option `treatEscapedChars` est activé. Ces caractères spéciaux sont les suivants :

- `$S` -> espace
- `$+` -> Dossier courant
- `$v` -> version du terminal
- `$N` -> saut de ligne

Pour parser les caractères, utilisez la méthode `zlShell->parseSpecialChars` :

	$a = $terminal->parseSpecialChars("Hello,\$SWorld!");
	fprintf(STDOUT, $a); // affichera "Hello, World!"

## Les options
<span id="__options"></span>

Il existe des options, qui modifient le fonctionnement du terminal. Ces options sont propres aux instances.

Pour modifier la valeur d'un option, utilisez la méthode `zlShell->setOption` :

	$terminal->setOption("treatSpecialChars", 0);

Pour obtenir la valeur d'un option, utilisez `zlShell->getOption` :

	$i = $terminal->getOption("treatSpecialChars");

Pour savoir si un option existe, utilisez `zlShell->optionExists` :

	$i = $terminal->optionExists("treatSpecialChars");

La méthode `zlShell->getOptions` retourne un tableau associatif contenant en clef le nom des options et en valeur leur valeur :

	$i = $terminal->getOptions();
	foreach ($i as $k => $v)
		fprintf(STDOUT, $k . " = " . $v);

## L'intro
<span id="__intro"></span>

L'intro est le string affiché lors du démarrage du terminal. Pour modifier l'intro, utilisez la méthode `zlShell->setIntro` :

	$terminal->setIntro("[Intro]$N");

Les caractères spéciaux sont autorisés.

Pour obtenir l'intro, utilisez `zlShell->getIntro` : 

	$i = $terminal->getIntro();

## La pile de commandes
<span id="__cmdspile"></span>

Lorsqu'une commande est entrée, elle est ajoutée à la pile de commandes.

A chaque tour de boucle du terminal, toutes les commandes dans la pile de commandes sont exécutées les unes après les autres puis ôtées de la pile. Pour ajouter une commande à la pile, utilisez la méthode `zlShell->appendCommand` :

	$terminal->appendCommand("help /?");

## Les commandes

Pour intégrer une commande à un terminal, utilisez la méthode `zlShell->registerCommand` :

	$terminal->registerCommand("help", new zlHelpCommand);