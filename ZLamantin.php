<?php
/*
 *     ZLamantin
 *   Copyright (C) 2019  Soni Tourret
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * ZLamantin console
 * 
 * Author : Soni TOURRET
 */
namespace com\darksidegames\zlamantin;

class zlUnknownOptionException extends \Exception {}

/**
 * La classe abstraite dont héritent les commandes.
 */
abstract class zlCommand
{
	/**
	 * Doit être surchargée, et contenir des string contenant les différentes formes de validité
	 * de la commande.
	 * 
	 * N'incluez pas dans les regex le nom de la commande.
	 * 
	 * Lors de l'appel de la commande, le terminal va vérifier à quel string la commande tapée
	 * correspond, et va mettre à disposition l'index de ce string, dans la fonction <code>action</code>
	 * <i>via</i> le paramètre <code>$maskIndex</code>. Vous pouvez placer des parenthèses
	 * capturantes dans ces regex pour captuer les paramètres, ils seront mis à disposition dans la
	 * fonction <code>action</code> <i>via</i> le paramètre <code>$maskArgs</code>.
	 * 
	 * Si la commande tapée ne correspond à aucune regex de ce tableau, la commande sera considérée
	 * comme non-valide, et la fonction <code>invalid</code> sera appelée, si elle est implémentée (via
	 * le trait <code>zlCustomInvalidationCommand</code>).
	 */
	protected $validityMasks = [];
	/**
	 * L'action de la commande. Doit être implémentée.
	 * 
	 * @param string $cmd Le nom de la commande dans le shell.
	 * @param string $arg Les arguments donnés à la commande.
	 * @param bool $stop Vaut `false`. Si changé à `true`, arrête la console.
	 * @param zlShell &$shell Une référence vers l'instance du shell dans lequel la commande est appelée.
	 * @param int $maskIndex L'index de <code>$validityMasks</code> auquel correspond la commande tapée.
	 * @param array $maskArgs Un tableau de string contenant les contenus des parenthèses capturantes de la regex à laquelle correspond la commande tapée.
	 * 
	 * @return void
	 */
	public abstract function action(string $cmd, string $arg, bool &$stop, zlShell &$shell, int $maskIndex, array $maskArgs);
	
	/**
	 * Retourne le tableau des formes de validité de la commande (<code>$validityMasks</code>).
	 * 
	 * @return array
	 */
	public function getValidityMasks()
	{
		return $this->validityMasks;
	}
}

/**
 * Le trait qu'implémentent les commandes documentées.
 */
trait zlDocumentedCommand
{
	/**
	 * La méthode appelée pour afficher la documentation de la commande. Doit être implémentée.
	 * 
	 * @param string $cmd Le nom de la commande dans le shell. Utilisez cette
	 * variable au lieu du nom de la commande pour que le nom de la commande
	 * dans l'aide reste le même que celui choisi dans la console.
	 * @param zlShell &$shell Une référence vers l'instance du shell dans
	 * lequel la commande est appelée.
	 * 
	 * @return void
	 */
	public abstract function help(string $cmd, zlShell &$shell);
}

/**
 * La classe abstraite dont héritent les sujets d'aide.
 */
abstract class zlHelpTopic
{
	public $name;
	/**
	 * La méthode appelée pour afficher la documentation. Doit être implémentée.
	 * 
	 * @param zlShell &$shell Une référence vers l'instance du shell dans
	 * lequel la commande est appelée.
	 * 
	 * @return void
	 */
	public abstract function help(zlShell &$shell);
}

/**
 * Le trait qu'implémentent les commandes pour posséder une méthode <code>invalid</code>, appelée
 * si la commande tapée ne correspond à aucun masque de validité.
 */
trait zlCustomInvalidationCommand
{
	/**
	 * La méthode appelée si la commande tapée ne correspond à aucun masque de validité.
	 * 
	 * @param string $cmd Le nom de la commande tapée.
	 * @param string $fullCmd La ligne de commande entière tapée.
	 * @param zlShell &$shell Une référence vers l'instance du shell dans lequel la commande
	 * est appelée.
	 * 
	 * @return void
	 */
	public abstract function invalid(string $cmd, string $fullCmd, zlShell &$shell);
}

class zlExitCommand extends zlCommand
{
	use zlDocumentedCommand;
	
	protected $validityMasks = [
		"/^$/",
		"/^\/\?$/"
	];
	
	public function action(string $cmd, string $arg, bool &$stop, zlShell &$shell, int $maskIndex, array $maskArgs)
	{
		if ($maskIndex === 0)
		{
			$stop = true;
			return;
		}
		elseif ($maskIndex === 1)
		{
			$this->help($cmd, $shell);
			return;
		}
	}
	
	public function help(string $cmd, zlShell &$shell)
	{
		fprintf(STDOUT, "    $cmd");
		fprintf(STDOUT, PHP_EOL);
		fprintf(STDOUT, "Ferme la console.");
		fprintf(STDOUT, PHP_EOL);
		fprintf(STDOUT, PHP_EOL);
		fprintf(STDOUT, "    $cmd /?");
		fprintf(STDOUT, PHP_EOL);
		fprintf(STDOUT, "Affiche ce message.");
		fprintf(STDOUT, PHP_EOL);
		return;
	}
}

class zlHelpCommand extends zlCommand
{
	use zlDocumentedCommand;
	
	protected $validityMasks = [
		"/^$/",
		"/^\/\?$/",
		"/^(.+)$/"
	];
	
	public function action(string $cmd, string $arg, bool &$stop, zlShell &$shell, int $maskIndex, array $maskArgs)
	{
		if ($maskIndex === 0)
		{
			// general help topics
			$undocumented = [];
			$documented = [];
			$helpTopics = [];
			
			foreach ($shell->getRegisteredCommands() as $k => $v)
			{
				if (method_exists($v, "help"))
					$documented[] = $k;
				else
					$undocumented[] = $k;
			}
			unset($k);
			unset($v);
			$helpTopics = $shell->getRegisteredHelpTopics();
			
			fprintf(STDOUT, "Documented commands (" . (string) count($documented) . ")" . PHP_EOL);
			fprintf(STDOUT, "----------" . PHP_EOL);
			$i = 1;
			foreach ($documented as $v)
			{
				fprintf(STDOUT, $v);
				if ($v !== $documented[count($documented) - 1]) // if it's the last element
					fprintf(STDOUT, ", ");
				if ($i === 5)
				{
					fprintf(STDOUT, PHP_EOL);
					$i = 1;
				}
				else
					$i++;
			}
			
			fprintf(STDOUT, PHP_EOL . "Undocumented commands (" . (string) count($undocumented) . ")" . PHP_EOL);
			fprintf(STDOUT, "----------" . PHP_EOL);
			$i = 1;
			foreach ($undocumented as $v)
			{
				fprintf(STDOUT, $v);
				if ($v !== $undocumented[count($undocumented) - 1])
					fprintf(STDOUT, ", ");
				if ($i === 5)
				{
					fprintf(STDOUT, PHP_EOL);
					$i = 1;
				}
				else
					$i++;
			}
			$i = null;
			$v = null;
			
			fprintf(STDOUT, PHP_EOL . "Help topics (" . (string) count($helpTopics) . ")" . PHP_EOL);
			fprintf(STDOUT, "----------" . PHP_EOL);
			$i = 0;
			$j = 0;
			foreach ($helpTopics as $k => $v)
			{
				fprintf(STDOUT, $k);
				if ($j !== count($helpTopics) - 1)
					fprintf(STDOUT, ", ");
				if ($i === 5)
				{
					fprintf(STDOUT, PHP_EOL);
					$i = 1;
				}
				else
					$i++;
				$j++;
			}
			unset($i);
			unset($j);
			unset($v);
			unset($k);
			
			fprintf(STDOUT, PHP_EOL . PHP_EOL);
		}
		elseif ($maskIndex === 1)
		{
			$this->help($cmd, $shell);
			return;
		}
		elseif ($maskIndex === 2)
		{
			// particular command help
			foreach ($shell->getRegisteredCommands() as $k => $v)
			{
				if ($maskArgs[0] === $k)
				{
					if (method_exists($v, "help"))
					{
						$v->help($k, $shell);
						return;
					}
					else
					{
						fprintf(STDOUT, "No help available for command : " . $maskArgs[0] . PHP_EOL);
						return;
					}
				}
			}
			unset($k);
			unset($v);
			foreach ($shell->getRegisteredHelpTopics() as $k => $v)
			{
				if ($maskArgs[0] === $k)
				{
					$v->help($shell);
					return;
				}
			}
			unset($k);
			unset($v);
			
			$shell->unknownCommand($maskArgs[0]);
		}
	}
	
	public function help(string $cmd, zlShell &$shell)
	{
		fprintf(STDOUT, "    $cmd" . PHP_EOL);
		fprintf(STDOUT, "Affiche la liste des commandes." . PHP_EOL);
		fprintf(STDOUT, PHP_EOL);
		fprintf(STDOUT, "    $cmd /?" . PHP_EOL);
		fprintf(STDOUT, "Affiche ce message." . PHP_EOL);
		fprintf(STDOUT, PHP_EOL);
		fprintf(STDOUT, "    $cmd <command>" . PHP_EOL);
		fprintf(STDOUT, "Affiche l'aide pour une commande en particulier." . PHP_EOL);
		return;
	}
}

/**
 * La classe qui définit un terminal.
 * 
 * Pour créer un terminal, il y a plusieurs façons.
 * La première est de créer une classe qui hérite de la classe <code>zlShell</code>, puis de créer
 * une instance ce cette classe, qui sera démarrée. Cette méthode a pour avantage de permettre la
 * redéfinition des méthodes natives de <code>zlShell</code>. Si vous n'avez pas besoin de
 * redéfinir les méthodes, orientez-vous plutôt vers la deuxième méthode.
 * La deuxième, plus simple, est de simplement créer une instance de la classe <code>zlShell</code>
 * qui sera démarrée.
 * 
 * Une fois un terminal créé, il faut lui intégrer des commandes. Pour cela, il suffit de créer une
 * classe qui hérite de la classe abstraite <code>zlCommand</code>, puis de l'enregistrer dans le
 * terminal avec la méthode <code>zlShell->registerCommand</code>.
 * 
 * De la même façon, on peut ajouter des topics d'aide. La seule différence est que les topics d'aide
 * n'héritent pas de la classe <code>zlCommand</code> mais de <code>zlHelpTopic</code>.
 * 
 * Notez que pour que l'aide soit disponible, il faut enregistrer dans le terminal la commande (déjà
 * créée) <code>zlHelpCommand</code>.
 * 
 * Une fois les commandes et les topics d'aide intégrés, il faut démarrer le terminal. Pour cela, il
 * faut appeler la méthode <code>zlShell->start</code>
 */
class zlShell
{
	protected $version= "1.0.0";
	
	protected $prompt = ">\$S";
	protected $cmdqueue = [];
	protected $intro = "[ZLamantin Shell v\$v]";
	protected $cmds = [];
	protected $helpTopics = [];

	protected $options = [
		"clearCommandsBufferAfterCommand" => 1,
		"debugMode" => 0,
		"treatEscapedChars" => 1
	];
	
	/**
	 * Parse les caractères spéciaux seulement si l'option <code>treatEscapedChars</code> est
	 * activé.
	 * 
	 * @param string $str Le string à parser.
	 * 
	 * @return string Le string parsé si l'option est activé, et le string de départ sinon.
	 */
	public function parseSpecialChars(string $str)
	{
		if ($this->getOption("treatEscapedChars") == 1)
		{
			$r = preg_replace("/\\\$S/", " ", $str);
			$r = preg_replace("/\\\$\\+/", getcwd(), $r);
			$r = preg_replace("/\\\$v/", $this->version, $r);
			$r = preg_replace("/\\\$N/", PHP_EOL, $r);
			return $r;
		}
		else
			return $str;
	}
	
	/**
	 * May be overrided
	 */
	public function __construct(string $intro = null)
	{
		if ($intro !=  null && $intro !== "")
			$this->intro = $intro;
		/* COMMANDES D'EXEMPLE, PAS ENREGISTREES PAR DEFAUT, MAIS ENREGISTRABLE
		$this->registerCommand("exit", new zlExitCommand);
		$this->registerCommand("help", new zlHelpCommand);
		*/
	}
	
	/**
	 * Démarre la console.
	 * 
	 * @return void
	 */
	public function start()
	{
		fprintf(STDOUT, $this->parseSpecialChars($this->intro));
		fprintf(STDOUT, PHP_EOL);
		
		$stop = false;
		while (!$stop)
		{
			fprintf(STDOUT, $this->parseSpecialChars($this->prompt));
			$line = fgets(STDIN);
			$line = trim($line);
			
			$this->appendCommand($line);
			$this->onCmd($stop);
		}
		
		fprintf(STDOUT, "Goodbye...");
		fprintf(STDOUT, PHP_EOL);
		
		return;
	}
	
	protected function onCmd(bool &$stop)
	{
		while (count($this->cmdqueue) !== 0)
		{
			$v = $this->cmdqueue[0];
			
			$splited = preg_split("/\s/", $v);
			$cmd = $splited[0];
			$arg = (count($splited) >= 2) ? $splited[1] : "";
			if (count($splited) > 2)
			{
				for ($i = 2; $i < count($splited); $i++)
					$arg .= " " . $splited[$i];
			}
			$splited = null;
			
			$unknown = true;
			
			foreach ($this->cmds as $k => $v2)
			{
				if ($cmd === $k)
				{
					$unknown = false;
					foreach ($v2->getValidityMasks() as $k3 => $v3)
					{
						$regexArgs = [];
						$tempRegexArgs = [];
						if (preg_match($v3, $arg, $tempRegexArgs))
						{
							if (count($tempRegexArgs) > 1)
							{
								for ($i = 1; $i < count($tempRegexArgs); $i++)
									$regexArgs[] = $tempRegexArgs[$i];
								unset($i);
							}
							unset($tempRegexArgs);
							$v2->action($cmd, $arg, $stop, $this, $k3, $regexArgs);
							break;
						}
						elseif ($k3 === count($v2->getValidityMasks()) - 1)
						{
							// si on a atteint la dernière regex et qu'elle n'est pas valide
							if (method_exists($v2, "invalid"))
							{
								$v2->invalid($cmd, $v, $this);
							}
							else
							{
								// invalidation par défaut
								$this->invalidCommand($cmd, $v);
							}
						}
						$regexArgs = null;
					}
				}
			}
			if ($unknown && $cmd !== "")
				$this->unknownCommand($cmd);
			
			array_shift($this->cmdqueue);
		}
		
		return;
	}
	
	/**
	 * La méthode appelée si on tape une commande existante, mais dans une forme qui n'est pas
	 * autorisée par les masques de validité de ladite commande. Notez que cette méthode ne
	 * sera appelée que si la classe de la commande n'implémente pas le trait
	 * <code>zlCustomInvalidationCommand</code>.
	 * 
	 * @param string $cmd Le nom de la commande passée.
	 * @param string $fullCmd La ligne de commande entière passée.
	 * 
	 * @return void
	 */
	public function invalidCommand(string $cmd, string $fullCmd)
	{
		fprintf(STDOUT, "Invalid command : $fullCmd" . PHP_EOL);
		return;
	}
	
	/**
	 * La méthode appelée si on tape une commande inexistante.
	 * 
	 * @param string $name Le nom de la commande.
	 * 
	 * @return void
	 */
	public function unknownCommand(string $name)
	{
		fprintf(STDOUT, "Unknown command : $name" . PHP_EOL);
		return;
	}
	
	/**
	 * Permet d'enregistrer une commande dans le terminal.
	 * 
	 * @param string $name Le nom qu'aura la commande dans ce terminal.
	 * @param zlCommand $command Une instance de la commande à enregistrer.
	 * 
	 * @return void
	 */
	public function registerCommand(string $name, zlCommand $command)
	{
		$this->cmds[$name] = $command;
		return;
	}
	
	/**
	 * Désenregistre une commande du terminal, si celle-ci a déjà été enregistrée.
	 * 
	 * @param string $name Le nom de la commande dans ce terminal.
	 * 
	 * @return bool <code>true</code> si la commande existait, et <code>false</code> sinon.
	 */
	public function unregisterCommand(string $name): bool
	{
		$ret = false;
		if ($this->isCommandRegistered($name))
		{
			unset($this->cmds[$name]);
			$ret = true;
		}
		
		return $ret;
	}
	
	/**
	 * Retourne si une commande a été enregistrée dans ce terminal.
	 * 
	 * @param string $name Le nom de la commande.
	 * 
	 * @return bool
	 */
	public function isCommandRegistered(string $name): bool
	{
		$ret = false;
		foreach ($this->cmds as $k => $v)
		{
			if ($k === $name)
			{
				$ret = true;
				break;
			}
		}
		unset($k);
		unset($v);
		return $ret;
	}
	
	/**
	 * Retourne un array des commandes enregistrées dans ce terminal.
	 * 
	 * @return array Un tableau associatif nom => instance
	 */
	public function getRegisteredCommands()
	{
		return $this->cmds;
	}
	
	/**
	 * Enregistre un topic d'aide dans le terminal.
	 * 
	 * Notez que l'aide ne sera disponible que si vous enregistrez la commande prédéfinie
	 * <code>zlHelpCommand</code> dans le terminal, ou que vous créez la votre.
	 * 
	 * @param zlHelpTopic $topic Une instance du topic d'aide.
	 * 
	 * @return void
	 */
	public function registerHelpTopic(zlHelpTopic $topic)
	{
		$this->helpTopics[$topic->name] = $topic;
		return;
	}
	
	/**
	 * Retourne si un topic d'aide a été enregistré dans le terminal.
	 * 
	 * @param string $topic Le nom du topic d'aide.
	 * 
	 * @return bool
	 */
	public function isHelpTopicRegistered(string $topic)
	{
		return isset($this->helpTopics[$topic]);
	}
	
	/**
	 * Désenregistre un topic d'aide du terminal, s'il a été enregistré dedans
	 * auparavant.
	 * 
	 * @param string $topic Le nom du topic d'aide.
	 * 
	 * @return bool <code>true</code> si le topic existait, et <code>false</code> sinon.
	 */
	public function unregisterHelpTopic(string $topic): bool
	{
		$ret = false;
		if ($this->isHelpTopicRegistered($topic))
		{
			unset($this->helpTopics[$topic]);
			$ret = true;
		}
		
		return $ret;
	}
	
	/**
	 * Retourne un array des topics d'aide enregistrés dans ce terminal.
	 * 
	 * @return array
	 */
	public function getRegisteredHelpTopics(): array
	{
		return $this->helpTopics;
	}
	
	/**
	 * Retourne le string utilisé comme prompt dans ce terminal.
	 * 
	 * @return string
	 */
	public function getPrompt(): string
	{
		return $this->prompt;
	}
	
	/**
	 * Change le string utilisé comme prompt dans ce terminal.
	 * 
	 * Vous pouvez utiliser les caractères spéciaux, qui seront parsés si l'option
	 * <code>treatSpecialChars</code> est activé.
	 * 
	 * @param string $newPrompt Le nouveau prompt.
	 * 
	 * @return void
	 */
	public function setPrompt(string $newPrompt)
	{
		$this->prompt = $newPrompt;
		return;
	}
	
	/**
	 * Ajoute une commande à la pile de commandes à exécuter.
	 * 
	 * @param string $line La ligne de commande.
	 * 
	 * @return void
	 */
	public function appendCommand(string $line)
	{
		$this->cmdqueue[] = $line;
		return;
	}
	
	/**
	 * Retourne la pile de commandes à exécuter.
	 * 
	 * @return array
	 */
	public function getCommandsBuffer()
	{
		return $this->cmdqueue;
	}

	/**
	 * Retourne si une option existe.
	 * 
	 * @param string $optionName Le nom de l'option.
	 * 
	 * @return bool
	 */
	public function optionExists(string $optionName): bool
	{
		return isset($this->options[$optionName]);
	}

	/**
	 * Change la valeur d'une option.
	 * 
	 * @param string $optionName Le nom de l'option.
	 * @param $value La nouvelle valeur de l'option.
	 * 
	 * @return void
	 * @throws zlUnknownOptionException Si l'option n'existe pas.
	 */
	public function setOption(string $optionName, $value)
	{
		if ($this->optionExists($optionName))
		{
			$this->options[$optionName] = $value;
			return;
		}
		else
			throw new zlUnknownOptionException($optionName . " is unknown.");
	}

	/**
	 * Retourne la valeur d'une option.
	 * 
	 * @param string $optionName Le nom de l'option.
	 * 
	 * @return La valeur de l'option.
	 * @throws zlUnknownOptionException Si l'option n'existe pas.
	 */
	public function getOption(string $optionName)
	{
		if ($this->optionExists($optionName))
			return $this->options[$optionName];
		else
			throw new zlUnknownOptionException($optionName . " is unknown.");
	}

	/**
	 * Retourne un array contenant la liste des options et leur valeur.
	 * 
	 * @return array Un tableau associatif avec pour clef le nom de l'option et pour valeur sa
	 * valeur.
	 */
	public function getOptions()
	{
		return $this->options;
	}
	
	/**
	 * Définit l'intro utilisée.
	 * 
	 * @param string $intro L'intro. Vous pouvez utiliser les caractères spéciaux, qui seront parsés si l'option <code>treatSpecialChars</code> est activé.
	 * 
	 * @return void
	 */
	public function setIntro(string $intro)
	{
		$this->intro = $intro;
		return;
	}
	
	/**
	 * Retourne l'intro utilisée.
	 * 
	 * @return string
	 */
	public function getIntro()
	{
		return $this->intro;
	}
}
?>